const socket = io('https://webrtc7576.herokuapp.com/');

let myPeerID;
let userMediaStream;
let videoTracks;
let audioTracks;

const userArea = (name, peerId) => {
    return (`
        <a id=${peerId} class="list-group-item list-group-item-action rounded-0">
            <div class="media">
                <img src="./src/avatar_usae7z.svg"
                    alt="user" width="30" class="rounded-circle">
                <div class="media-body ml-4" style="align-self: center;">
                    <div class="d-flex align-items-center justify-content-between">
                        <h6 class="mb-0">${name}</h6>
                    </div>
                </div>
            </div>
        </a>`
    )
}

// Ẩn khung chat video
$('#chatview').hide();

// Cập nhật lại danh sách người đang online
socket.on('DANH_SACH_NGUOI_DUNG_ONLINE', arrUsers => {
    $('#chatview').show();
    $('#loginview').hide();
    arrUsers.forEach(user => {
        const {name, peerId} = user;
        if (peerId != myPeerID) {
            $('#listuser').append(userArea(name, peerId))
        }
    });
    // Có người dung mới them vào
    socket.on('CO_NGUOI_DUNG_MOI', user => {
        const {name, peerId} = user;
        $('#listuser').append(userArea(name, peerId))
    });
});

// Đăng kí lỗi
socket.on('DANG_KY_THAT_BAI', () => alert("user đã tồn tại"));

// Ngắt kết nối
socket.on('AI_DO_NGAT_KET_NOI', peerId => {
    $(`#${peerId}`).remove();
});

// Play video
function playStream(idTag, stream) {
    const video = document.getElementById(idTag);
    video.srcObject = stream;
}

// Cho phép bật camera
function openStream() {
    var config = {
        video: true,
        audio: true,
        video: { width: 1920, height: 1080}
    }
    if (userMediaStream == null) {
        userMediaStream =  navigator.mediaDevices.getUserMedia(config);
        userMediaStream.then((e) => {
            videoTracks = e.getVideoTracks();
            audioTracks = e.getAudioTracks();
        }).catch(() => {
            alert("Error!")
        })
        
    }
    return userMediaStream;
}
// Tạo peer để kết nối 2 máy với nhau.
const peer = new Peer();
// Khi peer được khởi tạo
peer.on('open', id => {
    myPeerID = id;
    $('#btnsignin').val('Log in');
    $('#btnsignin').click(() => {
        const username = $('#username').val();
        socket.emit('SIGN_IN', {name: username, peerId: id});
        openStream().then(stream => {
            playStream('local', stream);
        });
    });
});



// Người trả lời.
peer.on('call', call => {
    openStream().
    then(stream => {
        call.answer(stream);
        call.on('stream', remoteStream => playStream('remote', remoteStream));
    });
});

// Click vào người dùng để gọi.
$('#listuser').on('click', 'a', function(){
    $("#listuser>a.active").removeClass("active");
    $(this).addClass('active');
    const remoteId = $(this).attr('id');
    openStream().then(stream => {
        const call = peer.call(remoteId, stream);
        call.on('stream', remoteStream => playStream('remote', remoteStream));
    });
});


$("#audiooff").click(() => {
    $("#audiooff>i").toggleClass("fa-microphone-slash");
    $("#audiooff").toggleClass("red-btn");
    audioTracks[0].enabled = !audioTracks[0].enabled;
})

$("#camoff").click(() => {
    $("#camoff>i").toggleClass("fa-video-slash");
    $("#camoff").toggleClass("red-btn");
    videoTracks[0].enabled = !videoTracks[0].enabled;
})